package com.example.joopaulo.ioasys_jp.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.joopaulo.ioasys_jp.API.Conexao;
import com.example.joopaulo.ioasys_jp.R;

/**
 * Created by João Paulo on 01/08/2017.
 */

public class EmpresaActivity extends AppCompatActivity {
    private TextView tvDetalhesEmpresa;
    private ImageView imgvEmpresa;
    private Conexao conexao;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_empresa);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        tvDetalhesEmpresa = (TextView) findViewById(R.id.tvDetalhesEmpresa);
        imgvEmpresa = (ImageView) findViewById(R.id.imgvEmpresa);
        conexao = new Conexao();
        Bundle bundle = getIntent().getExtras();
        String nomeEmpresa = bundle.getString("nome");
        String detalhesEmpresa = bundle.getString("detalhes");
        String photoURL =  bundle.getString("photo");
        setTitle(nomeEmpresa);
        tvDetalhesEmpresa.setMovementMethod(new ScrollingMovementMethod());
        tvDetalhesEmpresa.setText(detalhesEmpresa);

        Glide.with(this)
                .load(conexao.URLBase + photoURL).error(R.drawable.img_e_1_lista)
                .fitCenter()
                .centerCrop()
                .into(imgvEmpresa);

    }

}
