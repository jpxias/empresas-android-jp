package com.example.joopaulo.ioasys_jp.Models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by João Paulo on 08/08/2017.
 */

public class Login {
    @SerializedName("email")
    private String email;

    @SerializedName("password")
    private String senha;

    public Login(String email, String senha) {
        this.email = email;
        this.senha = senha;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }
}
