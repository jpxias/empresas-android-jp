package com.example.joopaulo.ioasys_jp.Activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.joopaulo.ioasys_jp.API.Conexao;
import com.example.joopaulo.ioasys_jp.API.EmpresaService;
import com.example.joopaulo.ioasys_jp.Models.Login;
import com.example.joopaulo.ioasys_jp.R;
import com.example.joopaulo.ioasys_jp.Models.Usuario;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class LoginActivity extends AppCompatActivity {
    private Login login;
    private EmpresaService empresaService;
    private ProgressDialog dialog;
    private Conexao conexao;
    private TextView editEmail,editSenha;
    Button bEntrar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        getSupportActionBar().hide();
        empresaService = conexao.conectarRetrofit();
        bEntrar = (Button) findViewById(R.id.bEntrar);
        editEmail = (TextView) findViewById(R.id.editEmail);
        editSenha = (TextView) findViewById(R.id.editSenha);
        bEntrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Logar(editEmail.getText().toString().trim(),editSenha.getText().toString().trim());
                //Logar("testeapple@ioasys.com.br","12341234");
            }
        });

    }


    public void Logar (String email, String senha){
        dialog = ProgressDialog.show(this, "Login",
                "Entrando", true);
        dialog.setCancelable(false);
        login = new Login(email, senha);
        Call call = empresaService.logar(login);
        call.enqueue(new Callback<Login>() {
            @Override
            public void onResponse(Call<Login> call, Response<Login> response) {
                if(response.isSuccessful()){
                    Usuario usuario = new Usuario(response.headers().get("access-token"), response.headers().get("client"),
                            response.headers().get("uid"));
                    if(usuario != null) {
                        Toast.makeText(getBaseContext(), "Bem-Vindo", Toast.LENGTH_SHORT).show();
                        Intent it = new Intent(LoginActivity.this, HomeActivity.class);
                        it.putExtra("acess_token", usuario.getAcess_token());
                        it.putExtra("client", usuario.getClient());
                        it.putExtra("uid", usuario.getUid());
                        dialog.dismiss();
                        startActivity(it);
                    }else{
                        Toast.makeText(LoginActivity.this, "Problema no servidor, tente mais tarde.", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(getBaseContext(), "Usuário ou senha incorretos!", Toast.LENGTH_SHORT).show();
                    dialog.dismiss();
                }
            }
            @Override
            public void onFailure(Call<Login> call, Throwable t) {
                Toast.makeText(getBaseContext(), "Erro no servidor, por favor tente novamente!", Toast.LENGTH_SHORT).show();
                dialog.dismiss();
            }
        });
    }



}
