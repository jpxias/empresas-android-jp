package com.example.joopaulo.ioasys_jp.Models;

/**
 * Created by João Paulo on 08/08/2017.
 */

public class TypeEmpresa {
    private Integer id;
    private String enterprise_type_name;

    public TypeEmpresa(Integer id, String enterprise_type_name) {
        this.id = id;
        this.enterprise_type_name = enterprise_type_name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getEnterprise_type_name() {
        return enterprise_type_name;
    }

    public void setEnterprise_type_name(String enterprise_type_name) {
        this.enterprise_type_name = enterprise_type_name;
    }
}
