package com.example.joopaulo.ioasys_jp.API;


import com.example.joopaulo.ioasys_jp.Models.ListEmpresas;
import com.example.joopaulo.ioasys_jp.Models.Login;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by João Paulo on 08/08/2017.
 */

public interface EmpresaService {

    @POST("users/auth/sign_in")
    Call<Login> logar(@Body Login login);

    @GET("enterprises")
    Call<ListEmpresas> listEmpresas(
            @Header("access-token") String acess_token,
            @Header("client") String client,
            @Header("uid") String uid,
            @Query("name") String empresa);
}


