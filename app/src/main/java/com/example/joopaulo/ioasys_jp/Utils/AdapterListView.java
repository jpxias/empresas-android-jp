package com.example.joopaulo.ioasys_jp.Utils;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;


import com.bumptech.glide.Glide;
import com.example.joopaulo.ioasys_jp.API.Conexao;
import com.example.joopaulo.ioasys_jp.Models.Empresa;
import com.example.joopaulo.ioasys_jp.R;

import java.util.List;

/**
 * Created by aluno on 27/03/17.
 */
public class AdapterListView extends BaseAdapter {
    private Activity activity;
    private LayoutInflater mInflater;
    private List<Empresa> itens;
    private Conexao conexao;

    public AdapterListView(Activity activity, List<Empresa> itens)
    {
        this.activity = activity;
        this.itens = itens;
        this.conexao = new Conexao();
    }

    /**
     * Retorna a quantidade de itens
     */
    public int getCount()
    {
        return itens.size();
    }

    /**
     * Retorna o item de acordo com a posicao dele na tela.
     */
    public Empresa getItem(int position)
    {
        return itens.get(position);
    }


    public long getItemId(int position)
    {
        return position;
    }

    public View getView(int position, View view, ViewGroup parent)
    {
        if(mInflater == null){
            mInflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        if(view == null){
           view = mInflater.inflate(R.layout.item_listview,null);
        }
            TextView tvNomeEmpresa = (TextView) view.findViewById(R.id.tvNomeEmpresa);
            TextView tvCategoria = (TextView) view.findViewById(R.id.tvCategoria);
            TextView tvLocalizacao = (TextView) view.findViewById(R.id.tvLocalizacao);
            ImageView img = (ImageView) view.findViewById(R.id.imagemview);


                // getting data for row
            Empresa empresa = itens.get(position);

            Glide.with(activity)
                .load(conexao.URLBase + empresa.getPhoto()).error(R.drawable.img_e_1_lista)
                .fitCenter()
                .centerCrop()
                .into(img);

            tvNomeEmpresa.setText(empresa.getEnterprise_name());
            tvCategoria.setText(empresa.getEnterprise_type().getEnterprise_type_name());
            tvLocalizacao.setText(empresa.getCountry());


        return view;
    }

}
