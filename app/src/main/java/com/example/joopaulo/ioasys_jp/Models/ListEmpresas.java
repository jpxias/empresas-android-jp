package com.example.joopaulo.ioasys_jp.Models;

import com.example.joopaulo.ioasys_jp.Models.Empresa;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by João Paulo on 08/08/2017.
 */

public class ListEmpresas implements Serializable {

    @SerializedName("enterprises")
    private ArrayList<Empresa> ListaEmpresas;

    public ListEmpresas() {
        this.ListaEmpresas = new ArrayList<Empresa>();
    }

    public ArrayList<Empresa> getListaEmpresas(){
        return ListaEmpresas;
    }
}
