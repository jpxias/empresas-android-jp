package com.example.joopaulo.ioasys_jp.Activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.joopaulo.ioasys_jp.API.Conexao;
import com.example.joopaulo.ioasys_jp.API.EmpresaService;
import com.example.joopaulo.ioasys_jp.Utils.AdapterListView;
import com.example.joopaulo.ioasys_jp.Models.Empresa;
import com.example.joopaulo.ioasys_jp.Models.ListEmpresas;
import com.example.joopaulo.ioasys_jp.R;
import com.example.joopaulo.ioasys_jp.Models.Usuario;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;



/**
 * Created by João Paulo on 31/07/2017.
 */

public class HomeActivity extends AppCompatActivity {
    private Usuario usuario;
    private EmpresaService empresaService;
    private ListView lvEmpresas;
    private Conexao conexao;
    private AdapterListView adapterListView;
    private TextView tvComecar;
    ProgressDialog dialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        getSupportActionBar().setDisplayShowCustomEnabled(true);
        empresaService = conexao.conectarRetrofit();
        View cView = getLayoutInflater().inflate(R.layout.action_bar, null);
        getSupportActionBar().setCustomView(cView);
        lvEmpresas = (ListView) findViewById(R.id.lvEmpresas);
        tvComecar = (TextView) findViewById(R.id.tvComecar);
        recebeUsuario();
    }

    public void recebeUsuario(){
        Bundle bundle = getIntent().getExtras();
        usuario = new Usuario(bundle.getString("acess_token"),bundle.getString("client"),bundle.getString("uid"));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.action_pesquisa, menu);

        SearchView mSearchView = (SearchView) menu.findItem(R.id.action_search)
                .getActionView();
        mSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                buscarEmpresa(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });

        mSearchView.setQueryHint("Pesquisar");
        return true;
    }



    public void buscarEmpresa(String busca) {
        if (usuario != null) {
            Call chamada = empresaService.listEmpresas(usuario.getAcess_token(), usuario.getClient(), usuario.getUid(),busca);
                chamada.enqueue(new Callback<ListEmpresas>() {
                    @Override
                    public void onResponse(Call<ListEmpresas> call, Response<ListEmpresas> response) {
                        mostrarEmpresas(response.body());
                    }

                    @Override
                    public void onFailure(Call call, Throwable t) {
                        Toast.makeText(getBaseContext(), "Erro no servidor, por favor tente novamente!", Toast.LENGTH_SHORT).show();
                    }
                });
        }
    }


    public void mostrarEmpresas(final ListEmpresas listEmpresas){
        tvComecar.setVisibility(View.INVISIBLE);
        adapterListView = new AdapterListView(this,listEmpresas.getListaEmpresas());
        lvEmpresas.setAdapter(adapterListView);
        lvEmpresas.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
                ///Pega o item que foi selecionado.
                Empresa item = adapterListView.getItem(arg2);
                Intent it = new Intent(HomeActivity.this, EmpresaActivity.class);
                it.putExtra("nome", item.getEnterprise_name());
                it.putExtra("detalhes", item.getDescription());
                it.putExtra("photo", item.getPhoto());
                // Toast.makeText(getBaseContext(), item.getCity()  , Toast.LENGTH_SHORT).show();
                startActivity(it);
            }

/*
        lvEmpresas.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(getBaseContext(), "Clicado!", Toast.LENGTH_SHORT).show();
                Intent it = new Intent(HomeActivity.this, EmpresaActivity.class);
                it.putExtra("id",listEmpresas);
            }
        });
        */
        });
    }

}
