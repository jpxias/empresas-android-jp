package com.example.joopaulo.ioasys_jp.Models;

/**
 * Created by João Paulo on 01/08/2017.
 */

public class Usuario {
    private String acess_token;
    private String client;
    private String uid;

    public Usuario(String acess_token, String client, String uid){
        this.acess_token = acess_token;
        this.client = client;
        this.uid = uid;
    }

    public String getAcess_token() {
        return acess_token;
    }

    public void setAcess_token(String acess_token) {
        this.acess_token = acess_token;
    }

    public String getClient() {
        return client;
    }

    public void setClient(String client) {
        this.client = client;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }
}
