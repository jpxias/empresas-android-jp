package com.example.joopaulo.ioasys_jp.API;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by João Paulo on 01/08/2017.
 */

public class Conexao {
    public static String URLBase = "http://54.94.179.135:8090";
    public static String URL = "http://54.94.179.135:8090/api/v1/";

    public static EmpresaService conectarRetrofit(){
             Retrofit retrofit = new Retrofit.Builder()
                             .baseUrl(URL)
                             .addConverterFactory(GsonConverterFactory.create())
                             .build();
             return retrofit.create(EmpresaService.class);
     }


}
